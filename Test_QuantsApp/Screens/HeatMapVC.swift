

import UIKit

class HeatMapVC: UIViewController {

    @IBOutlet weak var filter_CollectionView: UICollectionView!
    @IBOutlet weak var data_CollectionView: UICollectionView!
    var dataModel : HeatMapModel?
    var selectedFilterIndex = 0
    var viewModel : HeatMapViewModel?
    let btnInfoArr = [ButtonInfo(backColor: UIColor(rgb: 0xFFC300), txtColor: .black, title: "ALL", tag: 0),ButtonInfo(backColor: UIColor(rgb: 0x00FF00), txtColor: .black, title: "L",tag: 1),ButtonInfo(backColor: UIColor(rgb: 0xFFFF00), txtColor: .black, title: "SC", tag: 2),ButtonInfo(backColor: UIColor(rgb: 0xFF0000), txtColor: .black, title: "S", tag: 3),ButtonInfo(backColor: UIColor(rgb: 0x00D0F9), txtColor: .black, title: "LU", tag: 4)]
    
    var filterdQuoteDataArr : [QuoteInfo]?
    override func viewDidLoad() {
        super.viewDidLoad()
        updateFilterArray()
        initCollectionView()
        callHeatMapAPI()
    }

    func initCollectionView(){
        filter_CollectionView.delegate = self
        filter_CollectionView.dataSource = self
        data_CollectionView.delegate = self
        data_CollectionView.dataSource = self
        filter_CollectionView?.register(UINib(nibName: "FilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")
        data_CollectionView?.register(UINib(nibName: "DataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DataCollectionViewCell")

    }
    
    func callHeatMapAPI(){
        let Url = APIConstant.baseURL.rawValue + APIConstant.kSynopsisAPI.rawValue
        APIModule.getDataFromAPI(requestURL: Url, returnInCaseOffailure: true, decodingType: HeatMapModel.self) { (flag, result) in
            if let model : HeatMapModel = result{
                self.dataModel = model
                self.viewModel = HeatMapViewModel(heatmapModel: model)
                self.updateFilterArray()
                DispatchQueue.main.async {
                    self.data_CollectionView.reloadData()
                }
            }
        }
    }

    func updateFilterArray(){
        filterdQuoteDataArr = (selectedFilterIndex == 0) ? self.viewModel?.allDataArr : (selectedFilterIndex == 1) ? self.viewModel?.lDataArr : (selectedFilterIndex == 2) ? self.viewModel?.scDataArr : (selectedFilterIndex == 3) ? self.viewModel?.sDataArr : self.viewModel?.luDataArr
    }
}

extension HeatMapVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (collectionView == self.filter_CollectionView) ? btnInfoArr.count : filterdQuoteDataArr?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == self.data_CollectionView){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DataCollectionViewCell", for: indexPath as IndexPath) as! DataCollectionViewCell
            cell.setData(quotes: filterdQuoteDataArr?[indexPath.item])
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath as IndexPath) as! FilterCollectionViewCell
        cell.delegate = self
        cell.setDataForButton(btnInfo: btnInfoArr[indexPath.item],select: selectedFilterIndex)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView == self.filter_CollectionView) ? self.view.frame.size.width*0.32 : self.data_CollectionView.frame.size.width*0.1995
        let height = (collectionView == self.filter_CollectionView) ? self.filter_CollectionView.frame.size.height*0.8 : width
                return CGSize(width: width, height: height)
    }
}

extension HeatMapVC: FilterCollectionViewCellDelegate{
    func btnClicked(tag: Int) {
        self.selectedFilterIndex = tag
        updateFilterArray()
        self.filter_CollectionView.reloadData()
        self.data_CollectionView.reloadData()
    }
}

extension HeatMapVC: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let dataArr = (selectedFilterIndex == 0) ? self.viewModel?.allDataArr : (selectedFilterIndex == 1) ? self.viewModel?.lDataArr : (selectedFilterIndex == 2) ? self.viewModel?.scDataArr : (selectedFilterIndex == 3) ? self.viewModel?.sDataArr : self.viewModel?.luDataArr
        printLogger(searchText)
        if (searchText.count > 0){
        filterdQuoteDataArr = dataArr?.filter({ (obj) -> Bool in
            let tmp = obj.symbol
            return tmp.range(of: searchText, options: .caseInsensitive) != nil
        })}else{
            filterdQuoteDataArr = dataArr
        }
        self.data_CollectionView.reloadData()
        
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        updateFilterArray()
        self.data_CollectionView.reloadData()
        
    }
}
