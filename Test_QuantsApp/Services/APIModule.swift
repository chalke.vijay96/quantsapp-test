import Foundation


class APIModule: NSObject {

    class func getDataFromAPI<T:Decodable>(requestURL:String,returnInCaseOffailure:Bool,decodingType: T.Type,completion : @escaping(_ status:Bool,_ model:T?) -> Void)
    {
    LoadingIndicatorView.show()
    let request = URLRequest(url: URL(string: requestURL)!)
    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
        printLogger(response as Any)
        guard let data = data else { return }
        do{
            let model = try JSONDecoder().decode(T.self, from: data)
            completion(true, model)
        }catch{
         printLogger("CATCH")
        }
        DispatchQueue.main.async {
            LoadingIndicatorView.hide()
        }
    })

    task.resume()
    }
    
}

