
import Foundation
import UIKit

struct ButtonInfo {
    var backColor: UIColor
    var txtColor: UIColor
    var title: String
    var tag: Int
}

struct QuoteInfo {
    var symbol: String
    var price: String
    var openInterest: String
    var priceChange: String
    var openInterestChange: String
    var backColor: UIColor
    var colorValue: CGFloat
}
