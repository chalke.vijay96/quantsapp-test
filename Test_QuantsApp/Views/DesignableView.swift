
import Foundation
import UIKit

@IBDesignable
class DesignableView: UIView {
}
@IBDesignable
class DesignableImage: UIImageView {
}
@IBDesignable
class Designablebutton: UIButton {
}
extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable
    var roundArea : Bool
    {
        get {
            return true
        }
        set
        {
            self.layer.cornerRadius = self.frame.size.width / 2
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
@IBDesignable
class GradientView: UITableView {
    
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map {$0.cgColor}
        if (isHorizontal) {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint (x: 1, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint (x: 0.5, y: 1)
        }
    }
    
}


@IBDesignable
class IBDessignableLabel : UILabel {
    
    @IBInspectable override var text : String? {
        didSet {
            decorate()
        }
    }
    
    @IBInspectable var fontSize : CFloat = 16.0 {
        didSet {
            decorate()
        }
    }
    
    
    @IBInspectable var fontColor : UIColor? {
        didSet {
            decorate()
        }
    }
    
    func decorate() {
        
        guard let text = text else { return }
        
        let titleAttributess = [
            NSAttributedString.Key.font.rawValue : UIFont.systemFont(ofSize: CGFloat(fontSize)),
            NSAttributedString.Key.foregroundColor : fontColor ?? UIColor.black,
            NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue
            ] as! [NSAttributedString.Key : Any]
        
        let titleString = NSMutableAttributedString(string: text, attributes: titleAttributess)
        
        // Other attributes
        titleString.addAttribute(NSAttributedString.Key.kern, value: 0, range: NSRange(location: 0, length: text.count))
        
        self.attributedText = titleString
    }
}

