
import Foundation
import UIKit


public enum APPConstant : String {
    case ALLDATA = "ALL"
    case SDATA = "S"
    case SCDATA = "SC"
    case LUDATA = "LU"
    case LDATA = "L"
}
