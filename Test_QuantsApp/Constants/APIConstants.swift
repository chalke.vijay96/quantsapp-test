
import Foundation
import UIKit

public enum APIConstant : String {
    case baseURL = "https://qapptemporary.s3.ap-south-1.amazonaws.com/" // UAT
    case kSynopsisAPI = "test/synopsis.json"
}
