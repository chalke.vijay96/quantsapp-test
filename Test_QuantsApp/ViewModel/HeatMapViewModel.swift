

import Foundation
import UIKit

class HeatMapViewModel{
    let heatmapModel : HeatMapModel?
    var allDataArr : [QuoteInfo]?
    var lDataArr : [QuoteInfo]?
    var scDataArr : [QuoteInfo]?
    var sDataArr : [QuoteInfo]?
    var luDataArr : [QuoteInfo]?
    var lDataChangeVal : CGFloat = 0.0000
    var scDataChangeVal : CGFloat = 0.0000
    var sDataChangeVal : CGFloat = 0.0000
    var luDataChangeVal : CGFloat = 0.0000
    init(heatmapModel : HeatMapModel?) {
        printLogger("Enter in View Model")
        self.heatmapModel = heatmapModel
        self.allDataArr?.removeAll()
        self.lDataArr?.removeAll()
        self.scDataArr?.removeAll()
        self.sDataArr?.removeAll()
        self.luDataArr?.removeAll()
        getAllDataArrFor(indentifier: "L", completion: { dataArr in
            self.lDataArr = dataArr
            printLogger(self.lDataArr ?? "NO DATA IN lDataArr")
        })
        getAllDataArrFor(indentifier: "SC", completion: { dataArr in
            self.scDataArr = dataArr
            printLogger(self.scDataArr ?? "NO DATA IN scDataArr")
        })
        getAllDataArrFor(indentifier: "S", completion: { dataArr in
            self.sDataArr = dataArr
            printLogger(self.sDataArr ?? "NO DATA IN sDataArr")
        })
        getAllDataArrFor(indentifier: "LU", completion: { dataArr in
            self.luDataArr = dataArr
            printLogger(self.luDataArr ?? "NO DATA IN luDataArr")
        })
        getAllData(completion: { dataArr in
            self.allDataArr = dataArr
            printLogger(self.allDataArr ?? "NO DATA IN allDataArr")
        })
    }
    
    func getSingleQuote(quoteString : String, identifier : String) -> QuoteInfo?{
        let value = quoteString.components(separatedBy: ",")
        let basecolor = (identifier == APPConstant.LDATA.rawValue) ? UIColor(rgb: 0x00FF00) : (identifier == APPConstant.SCDATA.rawValue) ? UIColor(rgb: 0xFFFF00) : (identifier == APPConstant.SDATA.rawValue) ?UIColor(rgb: 0xFF0000) : (identifier == APPConstant.LUDATA.rawValue) ? UIColor(rgb: 0x00D0F9) : UIColor(rgb: 0xFFC300)
        let baseValue : CGFloat = (identifier == APPConstant.LDATA.rawValue) ? lDataChangeVal : (identifier == APPConstant.SCDATA.rawValue) ? scDataChangeVal : (identifier == APPConstant.SDATA.rawValue) ? sDataChangeVal : luDataChangeVal
        let chgPerStr = CGFloat((value[3] as NSString).doubleValue) * 100
        let valueF = chgPerStr/baseValue
        let firstUpdateVal = (valueF > 0.85) ? 0.0 : (valueF > 0.6) ? 0.05 : (valueF > 0.45) ? 0.1 : (valueF > 0.35) ? 0.15 : (valueF > 0.3) ? 0.2 : (valueF > 0.25) ? 0.25 : (valueF > 0.2) ? 0.3 : (valueF > 0.15) ? 0.35 : (valueF > 0.11) ? 0.4 : (valueF > 0.08) ? 0.45 : (valueF > 0.05) ? 0.5 : (valueF > 0.03) ? 0.55 : (valueF > 0.01) ? 0.6 : (valueF > 0.009) ? 0.65 : (valueF > 0.007) ? 0.7 : (valueF > 0.005) ? 0.73 : (valueF > 0.003) ? 0.75 : (valueF > 0.001) ? 0.78 : 0.8
        let secondUpdateVal = (chgPerStr < -0.5) ? 0.05 : (chgPerStr < -0.4) ? 0.1 : (chgPerStr < -0.3) ? 0.18  : (chgPerStr < -0.2) ? 0.2 : (chgPerStr < -0.1) ? 0.25 : (chgPerStr < -0.09) ? 0.3 : (chgPerStr < -0.07) ? 0.35 : (chgPerStr < -0.065) ? 0.45 : (chgPerStr < -0.05) ? 0.5 : 0.6
        printLogger("valueThree \(value[3])")
        printLogger("valueF \(valueF)")
        printLogger("Identifier \(identifier) chgPerStr \(chgPerStr) baseValue \(baseValue)")
        printLogger("updateVal \(firstUpdateVal)")
        let quotedata = QuoteInfo(symbol: value[0], price: value[1], openInterest: value[2], priceChange: value[3], openInterestChange: value[4], backColor: basecolor,colorValue: CGFloat((chgPerStr > -0.01) ? firstUpdateVal : secondUpdateVal))
        return quotedata
    }
    func getChangePercentage(quoteString : String, identifier : String) -> CGFloat{
        printLogger("QUOTE PRICE CHANGE \(quoteString)")
        let valueF = CGFloat((quoteString as NSString).floatValue)
        printLogger("QUOTE PRICE valueF \(valueF*100)")
        return valueF*100
    }
    func getAllData(completion: @escaping( ([QuoteInfo]?) -> () )){
        var dataArray = [QuoteInfo(symbol: "", price: "", openInterest: "", priceChange: "", openInterestChange: "", backColor: .white, colorValue: 1.0)]
        dataArray.removeAll()
        if let dataArr = heatmapModel?.l?.components(separatedBy: ";"){
            for i in 0..<dataArr.count {
                if let isdata = getSingleQuote(quoteString: dataArr[i], identifier: APPConstant.LDATA.rawValue){
                    dataArray.append(isdata)
                }
            }
            
        }
        if let dataArr = heatmapModel?.sc?.components(separatedBy: ";"){
            for i in 0..<dataArr.count {
                if let isdata = getSingleQuote(quoteString: dataArr[i], identifier: APPConstant.SCDATA.rawValue){
                    dataArray.append(isdata)
                }
            }
        }
        if let dataArr = heatmapModel?.s?.components(separatedBy: ";"){
            for i in 0..<dataArr.count {
                if let isdata = getSingleQuote(quoteString: dataArr[i], identifier: APPConstant.SDATA.rawValue){
                    dataArray.append(isdata)
                }
            }
        }
        if let dataArr = heatmapModel?.lu?.components(separatedBy: ";"){
            for i in 0..<dataArr.count {
                if let isdata = getSingleQuote(quoteString: dataArr[i], identifier: APPConstant.LUDATA.rawValue){
                    dataArray.append(isdata)
                }
            }
        }
        let sortedArray = dataArray.sorted(by: { $0.priceChange > $1.priceChange })
        completion(sortedArray)
        
    }
    
    func getAllDataArrFor(indentifier:String,completion: @escaping( ([QuoteInfo]?) -> () )){
        var dataArray = [QuoteInfo(symbol: "", price: "", openInterest: "", priceChange: "", openInterestChange: "", backColor: .white, colorValue: 1.0)]
        dataArray.removeAll()
        let dataString = (indentifier == APPConstant.LDATA.rawValue) ? heatmapModel?.l : (indentifier == APPConstant.SCDATA.rawValue) ? heatmapModel?.sc : (indentifier == APPConstant.SDATA.rawValue) ? heatmapModel?.s :  heatmapModel?.lu
        if let dataArr = dataString?.components(separatedBy: ";"){
            
            var testArray = [QuoteInfo(symbol: "", price: "", openInterest: "", priceChange: "", openInterestChange: "", backColor: .white, colorValue: 1.0)]
            testArray.removeAll()
            for i in 0..<dataArr.count {
                if let isdata = getSingleQuote(quoteString: dataArr[i], identifier: indentifier){
                    testArray.append(isdata)
                }
            }
            let testSortedArray = testArray.sorted(by: { $0.priceChange > $1.priceChange })
            setPercentageValue(identifier: indentifier, priceChange: testSortedArray[0].priceChange)
            
            for i in 0..<dataArr.count {
                if let isdata = getSingleQuote(quoteString: dataArr[i], identifier: indentifier){
                    dataArray.append(isdata)
                }
            }
            let sortedArray = dataArray.sorted(by: { $0.priceChange > $1.priceChange })
            completion(sortedArray)
        }else{
            completion(nil)
        }
    }
    
    func setPercentageValue(identifier:String, priceChange:String){
        switch identifier {
        case APPConstant.LDATA.rawValue:
            lDataChangeVal = getChangePercentage(quoteString: priceChange, identifier: APPConstant.LDATA.rawValue)
            printLogger("LDATACHANGEVALUE \(lDataChangeVal)")
        case APPConstant.SCDATA.rawValue:
            scDataChangeVal = getChangePercentage(quoteString: priceChange, identifier: APPConstant.SCDATA.rawValue)
            printLogger("SCDATACHANGEVALUE \(scDataChangeVal)")
        case APPConstant.SDATA.rawValue:
            sDataChangeVal = getChangePercentage(quoteString: priceChange, identifier: APPConstant.SDATA.rawValue)
            printLogger("SDATACHANGEVALUE \(sDataChangeVal)")
        case APPConstant.LUDATA.rawValue:
            luDataChangeVal = getChangePercentage(quoteString: priceChange, identifier: APPConstant.LUDATA.rawValue)
            printLogger("LUDATACHANGEVALUE \(luDataChangeVal)")
        default:
            break
        }
    }
}
