

import UIKit

protocol FilterCollectionViewCellDelegate{
    func btnClicked(tag:Int)
}

class FilterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn: UIButton!
    var delegate : FilterCollectionViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
 
    func setDataForButton(btnInfo:ButtonInfo, select:Int){
        DispatchQueue.main.async {
            self.baseView.clipsToBounds = true
            self.baseView.layer.cornerRadius = self.baseView.frame.height*0.5
            self.btn.tag = btnInfo.tag
            self.btn.setTitleColor(btnInfo.txtColor, for: .normal)
            self.btn.setTitle(btnInfo.title, for: .normal)
            self.lbl_title.font = UIFont(name: "HelveticaNeue-Bold", size: 25.0)!
            let color = (select == btnInfo.tag) ? btnInfo.backColor : btnInfo.backColor.withAlphaComponent(0.7)
            self.baseView.backgroundColor = color
            
        }
    }
    
    @IBAction func btnClickk(sender : UIButton) {
        self.delegate.btnClicked(tag: sender.tag)
    }
}
