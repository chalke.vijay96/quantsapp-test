
import UIKit

class DataCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var main_base_View: UIView!
    @IBOutlet weak var base_view: UIView!
    @IBOutlet weak var lbl_chgPercentage: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(quotes:QuoteInfo?){
        if let isQuote = quotes{
            self.lbl_Title.text = isQuote.symbol
            let chgPerStr = CGFloat((isQuote.priceChange as NSString).doubleValue) * 100
            self.lbl_chgPercentage.text = "\(String(format: "%.2f", chgPerStr))%"
            self.base_view.backgroundColor = UIColor.black.withAlphaComponent(quotes?.colorValue ?? 0.1)
            self.main_base_View.backgroundColor = isQuote.backColor
        }
    }
    
}
